resource "kubernetes_namespace" "namespace" {
  metadata {
    name = "${var.namespace_name}"
  }
}

resource "helm_release" "additionchart" {
  name       = "${var.helm_name}"
  repository = "${var.path_helm_chart_repo}"
  chart      = "${var.helm_chart_name}"
  version    = "${var.helm_chart_version}"
  namespace  = "${var.namespace_name}"
  timeout    = "${var.helm_chart_timeout}"
}