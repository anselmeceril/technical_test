variable "path_config_cluster_name" {
  type    = string
  default = "my-cluster"
}

variable "cluster_name" {
  type    = string
  default = "my-cluster"
}

variable "namespace_name" {
  type    = string
  default = "addition-test"
}


variable "helm_name" {
  type    = string
  default = "addition-test"
}


variable "path_helm_chart_repo" {
  type    = string
  default = "addition-test"
}

variable "helm_chart_name" {
  type    = string
  default = "addition-test"
}


variable "helm_chart_version" {
  type    = string
  default = "addition-test"
}

variable "helm_chart_timeout" {
  type    = string
  default = "addition-test"
}