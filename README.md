# technical_test

## Getting started

```
git clone https://gitlab.com/anselmeceril/technical_test.git
```

***

# Project guidelines

Deploy an instance of the PHP CMS Wordpress application and the MariaDB database server in our Kubernetes cluster.

We will use:
- Minikube to run the Kubernetes cluster locally on our computer
- terraform to define and manage our infrastructure as code
- helm to package our pre-configured Kubernetes resources (see wordpress_mariaDb)

## Installation

##### Install minikube

```
brew install minikube
```
##### Install Kubectl
```
brew install kubectl
```

## Cluster building
Creating a Kubernetes cluster with 3 nodes, each having 2 CPUs and 2 GB of RAM.

```
minikube start --nodes=3 --cpus=2 --memory=2g -p addition
```

```
% minikube -p addition status
addition
type: Control Plane
host: Running
kubelet: Running
apiserver: Running
kubeconfig: Configured

addition-m02
type: Worker
host: Running
kubelet: Running

addition-m03
type: Worker
host: Running
kubelet: Running
```

## Kubernetes infrastructure diagram
![Image Infra that will be deploy](./images/images_infra.png)


## Kubernetes command used to deploy all the cluster
```
terraform init
```
Terraform command to initialize the working directory containing Terraform configuration files.

```
terraform validate
```
Terraform command to validate the syntax and configuration of the Terraform files in the working directory.

```
terraform plan
```
Terraform command to create an execution plan that shows what actions Terraform will take.

```
terraform apply --auto-approve
```
Terraform command to apply the changes specified in the Terraform configuration files to your infrastructure.

## Justification of the Number of Pods

Two replicas of the wordpress and MariaDB have been created for:
- higher availability
- better fault tolerance.

These pods is spread across the 2 worker nodes of our minikube cluster.

By having multiple replicas of the WordPress and MariaDB pods, Kubernetes ensure that at least one replica of each pod is always available and handling requests.

For example if a node fails, the pod running on that node can be rescheduled on another node in the cluster, so that the application continues to function without any significant downtime.

Another reason why I had 2 replicas is that having multiple replicas can also help with scaling the application

## Justification of CPU Resource Allocation (Request and Limit)



For this part, I recommend installing 'metric-server' to get a view of resource consumption within the cluster. To do this, I have added a components.yaml file that installs the tool.

To install metric-server:

```
cd ./test_kubernetes_config
kubectl apply -f components.yaml
```

Check the current CPU and memory usage of all pods in a namespace
```
kubectl top pods -n addition-test
```

check the current CPU and memory usage of all nodes in the cluster
```
kubectl top nodes

```
###### Wordpress

```
resources:
    requests:
        memory: 300Mi
        cpu: 600m
    limits:
        memory: 700Mi
        cpu: 1200m
```

###### MariaDb

```
resources:
    requests:
        memory: 800Mi
        cpu: 300m
    limits:
        memory: 1000Mi
        cpu: 600m
```

###### Resources Request:

Concerning the the ressources request, i give wordpress more CPU as the mariaDB database because wordPress is a web application and may require more CPU resources to handle web server requests and render pages.
If the application has high traffic it will consume more cpu than mariadb database

But concerning the memory, MariaDB is a database management system and may require more memory resources to store and manage data.

###### Resources Limite:

The resource limits for a pod depend on the resources available on the node that the pod is scheduled on.

```
--cpus=2 --memory=2g 
```

That's why the combined resource limit of Wordpress and mariaDb pods does not exceed the resources of the node.


## Start wordpress by using the Wordpress kubernetes service

```
minikube -p addition service wordpress -n addition-test
```

![wordpress screen](./images/wordpress_screen.png)


## get password MariaDb

```
kubectl get secret mariadb -n addition-test -o jsonpath='{.data.password}'
echo <password> | base64 --decode
```

## check MYSQL wordpress database

kubectl exec -it <mariadbpod> -n addition-test -- /bin/bash

```
mysql -uroot -p<password_decoded>
show databases;
```

![mysql database](./images/mariadb.png)

## View infra

###### View pods
```
kubectl get pods -n addition-test
NAME                            READY   STATUS    RESTARTS   AGE
deploymariadb-f58677cd4-2hx7p   1/1     Running   0          10h
deploymariadb-f58677cd4-m8j2l   1/1     Running   0          10h
wordpress-7d464596c7-kqtjg      1/1     Running   0          10h
wordpress-7d464596c7-zqqm5      1/1     Running   0          10h
```

###### View services
```
kubectl get svc -n addition-test
NAME        TYPE        CLUSTER-IP       EXTERNAL-IP   PORT(S)        AGE
mariadb     ClusterIP   10.107.202.162   <none>        3306/TCP       10h
wordpress   NodePort    10.100.40.55     <none>        80:31000/TCP   10h
```

###### View secrets
```
kubectl get secrets -n addition-test

NAME                                     TYPE                 DATA   AGE
mariadb                                  Opaque               1      10h
sh.helm.release.v1.wordpressmariadb.v1   helm.sh/release.v1   1      10h
```

###### View config-map
```
kubectl get cm -n addition-test

NAME               DATA   AGE
kube-root-ca.crt   1      10h
mariadb            2      10h
```

###### View Persisten Volume Claim
```
kubectl get pvc -n addition-test

NAME      STATUS   VOLUME                                     CAPACITY   ACCESS MODES   STORAGECLASS   AGE
mariadb   Bound    pvc-ad058f6b-151d-497d-95b6-18e3100b7828   1Gi        RWO            standard       10h
```