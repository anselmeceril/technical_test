output "namespace_name" {
  value = kubernetes_namespace.namespace.metadata[0].name
}

output "helm_release_name" {
  value = helm_release.additionchart.name
}