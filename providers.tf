terraform {
  required_providers {
    kubernetes = {
      source = "hashicorp/kubernetes"
      version = "2.18.0"
    }
  }
}

provider "kubernetes" {
  config_path    = "${var.path_config_cluster_name}"
  config_context = "${var.cluster_name}"
}

provider "helm" {
  kubernetes {
	  config_path    = "${var.path_config_cluster_name}"
    config_context = "${var.cluster_name}"
	
  }
}